%global __python __python3

Name:           solana-autobalance
Version:        0.0.3
Release:        1%{?dist}
Summary:        Tool to automatically top up Solana balance

License:        MIT
URL:            https://gitlab.com/im-0/solana-autobalance
BuildArch:      noarch
Source0:        %{name}-%{version}.tar.gz

BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  systemd

Requires:       solana-mainnet-cli

%{?systemd_requires}


%description
Tool to automatically top up Solana balance.


%prep
%autosetup -p1 -n %{name}-%{version}


%build
%py3_build


%install
mkdir -p %{buildroot}/%{_unitdir}
mkdir -p %{buildroot}/%{_sharedstatedir}/solana-autobalance
mkdir -p %{buildroot}/%{_sysconfdir}/solana-autobalance

%py3_install


%files
%license LICENSE
%doc README.md
%{_bindir}/solana-autobalance
%{_unitdir}/solana-autobalance.service
%{_unitdir}/solana-autobalance.timer

%{python3_sitelib}/*

%attr(0750,solana-autobalance,solana-autobalance) %dir %{_sharedstatedir}/solana-autobalance
%attr(0750,root,solana-autobalance) %dir %{_sysconfdir}/solana-autobalance
%attr(0640,root,solana-autobalance) %{_sysconfdir}/solana-autobalance/config.ini.example


%pre
getent group solana-autobalance >/dev/null || groupadd -r solana-autobalance
getent passwd solana-autobalance >/dev/null || \
        useradd -r -s /sbin/nologin -d /var/lib/solana-autobalance -M \
        -c 'Solana autobalance' -g solana-autobalance solana-autobalance
exit 0


%changelog
* Tue Jan 4 2022 Ivan Mironov <mironov.ivan@gmail.com> - 0.0.3-1
- Update to 0.0.3

* Tue Jul 13 2021 Ivan Mironov <mironov.ivan@gmail.com> - 0.0.2-1
- Update to 0.0.2

* Mon May 10 2021 Ivan Mironov <mironov.ivan@gmail.com> - 0.0.1-1
- Initial release
